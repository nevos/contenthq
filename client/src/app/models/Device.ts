export class Device {
    type: string;
    ratios: number[] = [];
    params: String[] = [];
    template: string;

    constructor(type: string, ratios, params, template: string) {
        this.type = type;
        this.ratios = ratios;
        this.params = params;
        this.template = template;
    }
}
