import { Device } from './Device';

export class DevicesData {
    constructor(public devices: Device[]) {}
}
