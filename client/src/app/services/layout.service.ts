import { Injectable } from '@angular/core';
import { DatabaseService } from 'src/app/services/database.service';

@Injectable()
export class LayoutService {

  type: string = 'default-preview'
  constructor(private databaseService: DatabaseService) { }
  public async selectLayout(name) {
    return this.databaseService.getLayout(name) ;
  }

  public selectDevice(viewportWidth) {
    return viewportWidth <= 480 ? 'Mobile' : 'Desktop' ;
  }


}
