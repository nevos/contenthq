import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Device } from '../models/Device';
import { DevicesData } from '../models/DevicesData';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  static USERS_KEY = 'users';
  static SERVER_URL = 'http://localhost:3000/';
  static GET_LAYOUT = 'layout';
  static SET_LAYOUT = 'layout';

  constructor(private http: HttpClient) {
  }

  /**
   * Gets layout from server
   *
   * @returns A promise for a UsersData object with the server users
   */
  public async getLayout(name) {
    try {
      const layout = await this.http.get<Device>(DatabaseService.SERVER_URL + DatabaseService.GET_LAYOUT + "/" + name).toPromise();
      console.log(layout) ;
      return layout;
    } 
    catch {
      console.log("error conecting to server")
      alert('DB ERROR - failed to connect to server') ;
    }
  }

    /**
   * Deletes layout from server using client id
   *
   * @returns A promise for a deleted object
   */
  public async deleteLayout(id) {
    const layout = await this.http.delete<Device>(DatabaseService.SERVER_URL + DatabaseService.GET_LAYOUT + "/" + id).toPromise();
    console.log(layout) ;
    return layout;
  }

  /**
   * Gets layout from server
   *
   * @returns A promise for a Device[] object with the server Devices
   */
  public async getAllLayouts() {
    try {
      const layouts = await this.http.get<Device[]>(DatabaseService.SERVER_URL + DatabaseService.GET_LAYOUT).toPromise();
      console.log(layouts) ;
      return new DevicesData(layouts);
    } 
    catch {
      console.log("error conecting to server")
    }
  }

  /**
   * Adds layout in server using Device object
   *
   * @returns A promise for a layout set.
   * */
  public async setLayout(device) {
    try {
      const layout = await this.http.post<Device>(DatabaseService.SERVER_URL + DatabaseService.SET_LAYOUT + '/' + device.type, device).toPromise();
      console.log(layout) ;
      return layout;
    }
    catch {
      console.log("error conecting to server");
    }
  }
}
