import {
    ComponentFactoryResolver,
    Injectable,
    Inject,
    Type,
  } from '@angular/core'
  
import { DesktopTplAComponent } from '../components/templates/desktop-tpl-a/desktop-tpl-a.component';
import { DesktopTplBComponent } from '../components/templates/desktop-tpl-b/desktop-tpl-b.component';
import { MobileTplAComponent } from '../components/templates/mobile-tpl-a/mobile-tpl-a.component';
import { MobileTplBComponent } from '../components/templates/mobile-tpl-b/mobile-tpl-b.component';

  @Injectable()
  export class LoaderService {
    factoryResolver ;
    rootViewContainer ;
    constructor(@Inject(ComponentFactoryResolver) factoryResolver) {
      this.factoryResolver = factoryResolver;
    }
    setRootViewContainerRef(viewContainerRef) {
      this.rootViewContainer = viewContainerRef
    }
    addDynamicComponent(componentName) {
      const factories = Array.from(this.factoryResolver['_factories'].keys());
      const factoryClass = <Type<any>>factories.find((x: any) => x.name === componentName);
      const factory = this.factoryResolver
                          .resolveComponentFactory(factoryClass)
      const component = factory
        .create(this.rootViewContainer.parentInjector)
      this.rootViewContainer.insert(component.hostView)
    }
  }