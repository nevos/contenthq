import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PreviewService {

  type: string = 'default-preview'


  selectPreview(row) {
    this.type = row.type == 'mobile' || row.type == 'desktop' ? row.type + '-preview' : 'default-preview';
    console.log("preview service:::", this.type)
    console.log("selectPreview", row)
  }

  constructor() { }
}
