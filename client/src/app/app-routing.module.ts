import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContentComponent } from './components/content/content.component';
import { AdminComponent } from './components/admin/admin.component';

const routes: Routes = [
{ path: '', redirectTo: '/content', pathMatch: 'full' },
{ path: 'content', component: ContentComponent },
{ path: 'admin', component: AdminComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
