import { Component, OnInit,Input, HostListener } from '@angular/core';
import { DevicesData } from '../../models/DevicesData';
import { Device } from '../../models/Device';
import { DatabaseService } from 'src/app/services/database.service';
import { PreviewService } from 'src/app/services/preview.service';
import { MatSnackBar } from '@angular/material';
import { PreviewComponent } from '../preview/preview.component';


@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.scss']
})
export class ClientsListComponent implements OnInit {

  DevicesData: DevicesData;

  selectedRow: -1;

  constructor(private previewService: PreviewService,private databaseService: DatabaseService, public snackBar: MatSnackBar) { }
  
  @Input() preview: PreviewComponent;

  ngOnInit() {
    this.getAllLayouts() ;
  }

  selectRow(selectedRow,i) {
    this.selectedRow = i ;
    this.preview.selectPreview(selectedRow) ;
    this.previewService.selectPreview(selectedRow) ;
    return true;
  }

  async getLayout(client: string) {
    try {
      let DevicesData = await this.databaseService.getLayout(client);
      return DevicesData ;
    } catch {
      this.snackBar.open('An error occured :(');
    }
  }

  async deleteDevice(device,i) {
    try {
      console.log("deleteDevice",device,i) ;
      await this.databaseService.deleteLayout(device._id);
      this.getAllLayouts();
      this.snackBar.open('Deleted device','', {
        duration: 3000
      });
    } catch(err) {
      this.snackBar.open('An error occured :(', err);
    }
  }

  async getAllLayouts() {
    console.log("in getAllLayouts") ;
    try {
      this.DevicesData = await this.databaseService.getAllLayouts();
      return this.DevicesData ;
    } catch {
      this.snackBar.open('An error occured :(');
    }

  }

  async setParams(device) {
    try {
      let clientLayout = await this.databaseService.setLayout(device);
      this.snackBar.open('set Params!','', {
        duration: 3000
      });
      console.log(clientLayout) ;
    } catch {
      this.snackBar.open('An error occured :(');
    }
  }
}
