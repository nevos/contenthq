import { Component, OnInit, Input, HostListener } from '@angular/core';
import { Device } from 'src/app/models/Device';
import { DatabaseService } from 'src/app/services/database.service';
import { MatSnackBar } from '@angular/material';
import { ClientsListComponent } from '../clients-list/clients-list.component';

@Component({
  selector: 'app-add-device',
  templateUrl: './add-device.component.html',
  styleUrls: ['./add-device.component.scss']
})
export class AddDeviceComponent implements OnInit {

  device: Device;
  @Input() clientsList: ClientsListComponent;

  
  constructor(private databaseService: DatabaseService, public snackBar: MatSnackBar) {
    this.device = new Device('',[50,50],[],'');
  }

  ngOnInit() {
  }

  inputRatioNum = 50 ;
  onRatiosKey(val) {
    val == undefined ? this.inputRatioNum = 0 : this.inputRatioNum = val  ;
//    console.log("ON KEY EVENT", this.inputRatioNum);
    this.device.ratios = [this.inputRatioNum,100-this.inputRatioNum]

  }

  async sendDevice() {
    try {
      if (this.device.type === '' || this.device.ratios === [] || this.device.params === []) {
        this.snackBar.open('Please fill all fields','', {
          duration: 3000
        });
        return true;
      }
      await this.databaseService.setLayout(new Device(this.device.type, this.device.ratios,this.device.params,''));
      this.clientsList.getAllLayouts();
      this.snackBar.open('Submitted device','', {
        duration: 3000
      });
    } catch(err) {
      this.snackBar.open('An error occured :(', err);
    }
  }

}

