import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/services/database.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {


  clientLayout: any;

  constructor(private databaseService: DatabaseService, public snackBar: MatSnackBar) {
  }

  ngOnInit() {
  }

}
