import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})
export class PreviewComponent implements OnInit {

  type: string = 'default-preview'
  
  
  listOfOptions = [
      {"name": "some name 1", ID: "D1", "checked": true},
      {"name": "some name 2", ID: "D2", "checked": false}
  ]

  
    constructor() { }

  ngOnInit() {
  }


  public selection: string;
  
  
  selectPreview(row) {
    console.log("selectPreview", row)
    this.type = row.type == 'mobile' || row.type == 'desktop' ? row.type + '-preview' : 'default-preview';
  }
}
