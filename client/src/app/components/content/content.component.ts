import { Component, Input, OnInit, ViewChild, OnDestroy, HostListener, Inject, ViewContainerRef } from '@angular/core';
import { LayoutService } from 'src/app/services/layout.service';
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute, Router } from "@angular/router";
import { LoaderService } from '../../services/loader-service';


@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
  providers: [LayoutService,LoaderService]
})
export class ContentComponent implements OnInit, OnDestroy {
  snapshotParam = "initial value";
  subscribedParam = "initial value";
  screenHeight = 0;
  screenWidth = 0;
  @HostListener('window:resize', ['$event'])

  onResize(event?) {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;  
  }

  constructor(public snackBar: MatSnackBar,
              private readonly route: ActivatedRoute,
              private readonly router: Router,
              private layoutService: LayoutService,
              @Inject(LoaderService) loaderService,
              @Inject(ViewContainerRef) viewContainerRef) {
                loaderService.setRootViewContainerRef(viewContainerRef)

                this.onResize();
                console.log("current sizes:", this.screenWidth, this.screenHeight) ;
                let layoutDeviceType = layoutService.selectDevice(this.screenWidth) ;
                  let layoutPromise = layoutService.selectLayout(layoutDeviceType.toLowerCase()) ;
                  layoutPromise.then(tpl => 
                    {
                      let templateName = !tpl || !tpl.template ? 'DefaultTplComponent' :   
                            layoutDeviceType +  'Tpl'+ tpl.template +'Component' ;

                      console.log("template to load", templateName) ;
                      loaderService.addDynamicComponent(templateName) ;    
                    })
                .catch( err => {});          
                
  }

  ngOnDestroy() {}
  ngOnInit() {

    // // No Subscription
    // this.snapshotParam = this.route.snapshot.paramMap.get("content");

    // // Subscribed
    // this.route.paramMap.subscribe(params => {
    //   this.subscribedParam = params.get("content");
    // });
  }

  // goto(content: string): void {
  //   this.router.navigate(["content", content]);
  // }

    /*
     * Exmple for query parameters is relatively the same as above.
     *  
     * this.snapshotQueryParam = this.route.snapshot.queryParamMap.get("queryName");
     * this.route.queryParamMap.subscribe(queryParams => {
     *   this.subscribedQueryParam = queryParams.get("queryName");
     * });
     */
}
