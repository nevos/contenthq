import { Component, OnInit } from '@angular/core';
import { Device } from '../../models/Device';

@Component({
  selector: 'app-device-item',
  templateUrl: './device-item.component.html',
  styleUrls: ['./device-item.component.scss']
})
export class DeviceItemComponent implements OnInit {

  Device: Device; 

  constructor(Device) { 
    this.Device = Device ;
  }

  ngOnInit() {
  }

}
