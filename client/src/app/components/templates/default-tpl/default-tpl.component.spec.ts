import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultTplComponent } from './default-tpl.component';

describe('DefaultTplComponent', () => {
  let component: DefaultTplComponent;
  let fixture: ComponentFixture<DefaultTplComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefaultTplComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultTplComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
