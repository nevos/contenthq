import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileTplAComponent } from './mobile-tpl-a.component';

describe('MobileTplAComponent', () => {
  let component: MobileTplAComponent;
  let fixture: ComponentFixture<MobileTplAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileTplAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileTplAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
