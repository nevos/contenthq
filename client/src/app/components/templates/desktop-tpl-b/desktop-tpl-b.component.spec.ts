import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesktopTplBComponent } from './desktop-tpl-b.component';

describe('DesktopTplBComponent', () => {
  let component: DesktopTplBComponent;
  let fixture: ComponentFixture<DesktopTplBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesktopTplBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesktopTplBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
