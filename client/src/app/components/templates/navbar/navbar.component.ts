import { Component, OnInit } from '@angular/core';
import { PreviewService } from 'src/app/services/preview.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  type: string = 'default-preview'

  navbarOpen = false;
  previewType() {
    this.type = this.previewService.type ;
    return this.previewService.type ;
  }


  constructor(private previewService: PreviewService) { }

  ngOnInit() {
  }

  selectPreview(row) {
  }

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }

}
