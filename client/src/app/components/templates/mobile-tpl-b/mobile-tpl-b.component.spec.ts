import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileTplBComponent } from './mobile-tpl-b.component';

describe('MobileTplBComponent', () => {
  let component: MobileTplBComponent;
  let fixture: ComponentFixture<MobileTplBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileTplBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileTplBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
