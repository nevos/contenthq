import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-desktop-tpl-a',
  templateUrl: './desktop-tpl-a.component.html',
  styleUrls: ['./desktop-tpl-a.component.scss']
})
export class DesktopTplAComponent implements OnInit {
  
  @Input() data: any;
  
  constructor() { }

  ngOnInit() {
  }

}
