import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesktopTplAComponent } from './desktop-tpl-a.component';

describe('DesktopTplAComponent', () => {
  let component: DesktopTplAComponent;
  let fixture: ComponentFixture<DesktopTplAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesktopTplAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesktopTplAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
