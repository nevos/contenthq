import { DatabaseService } from './services/database.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { MatToolbarModule, MatListModule, MatIconModule, MatButtonModule, MatExpansionModule, MatFormFieldModule,
  MatInputModule, MatSliderModule, MatSnackBarModule, MatRadioModule} from '@angular/material';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClientsListComponent } from './components/clients-list/clients-list.component';
import { AddDeviceComponent } from './components/add-device/add-device.component';
import { ContentComponent } from './components/content/content.component';
import { AdminComponent } from './components/admin/admin.component';
import { PreviewComponent } from './components/preview/preview.component';
import { MobileTplAComponent } from './components/templates/mobile-tpl-a/mobile-tpl-a.component';
import { MobileTplBComponent } from './components/templates/mobile-tpl-b/mobile-tpl-b.component';
import { DesktopTplAComponent } from './components/templates/desktop-tpl-a/desktop-tpl-a.component';
import { DesktopTplBComponent } from './components/templates/desktop-tpl-b/desktop-tpl-b.component';
import { NavbarComponent } from './components/templates/navbar/navbar.component';
import { ProductComponent } from './components/templates/product/product.component';
import { SidebarComponent } from './components/templates/sidebar/sidebar.component';
import { ReviewsComponent } from './components/templates/reviews/reviews.component';
import { DefaultTplComponent } from './components/templates/default-tpl/default-tpl.component';


@NgModule({
  declarations: [
    AppComponent,
    ClientsListComponent,
    AddDeviceComponent,
    ContentComponent,
    AdminComponent,
    PreviewComponent,
    MobileTplAComponent,
    MobileTplBComponent,
    DesktopTplAComponent,
    DesktopTplBComponent,
    NavbarComponent,
    ProductComponent,
    SidebarComponent,
    ReviewsComponent,
    DefaultTplComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatSliderModule,
    MatRadioModule,
    HttpClientModule
  ],
  providers: [DatabaseService],
  bootstrap: [AppComponent],
  entryComponents: [DefaultTplComponent,DesktopTplAComponent,DesktopTplBComponent,MobileTplAComponent,MobileTplBComponent],
})
export class AppModule { }
