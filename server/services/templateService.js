"use strict";

// Food is a base class
class templateSelect {
    constructor (client) {
        if(!client || !client[0]) return null;
        this.client = client[0].toObject();
        this.client.template = Math.floor((Math.random() * 100) + 1) <= this.client.ratios[0] ? 'A':'B' ; 
    }

    toString () {
        return this.client ;
    }

}

exports.templateSelect = templateSelect;
