var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var layoutRoute = require('./routes/layout');

var app = express();

// Automatically parses JSON bodies of requests
app.use(bodyParser.json());
app.use((req, res, next) => {
    // For local testing purposes
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
    next();
});

// Add routes
app.use('/layout', layoutRoute);

// Server init
app.listen(3000, function () {
    // Assuming a local mongodb instance with the db contentHQDB
    mongoose.connect('mongodb://localhost/ContenHQDB')
        .then(() => console.log('mongodb: connection succesful'))
        .catch((err) => console.error(err));
    console.log('express: listening on 3000');
});

