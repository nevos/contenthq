var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ClientSchema = new Schema({
    type: {
        type: String,
        index: true,
        unique: true,
    },
    ratios: [Number],
    params: [String]
}, { versionKey: false });

var Client = mongoose.model('Client', ClientSchema);
module.exports = Client;