var express = require('express');
var router = express.Router();
var Client = require('../models/client');
let templateService = require('../services/templateService');
var mongoose = require('mongoose') ;


router.post('/:type', async (req, res) => {
    const body = req.body ;
    const type = req.params.type ;
    
    try {
        await Client.findOneAndUpdate({ type: type }, { $set: {ratios: body.ratios, params: body.params} }, { upsert: true });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
    res.json({message: 'updated'})
});

router.get('/:type', (req, res) => {
    const type = req.params.type ;

    Client.find({type: type}, function (err, data) {
        if (err) {
        res.status(500).json({ message: err.message })
        }
        // get the random function to get display based on client type.
        let client = new templateService.templateSelect(data) ;
        res.json(client ? client.client : null);
    });    
});

router.get('/', (req, res) => {
    Client.find(function (err, data) {
        if (err) {
            res.status(500).json({ message: err.message })
        }
        res.json({display: data});
    });    
});

router.delete('/:id', (req, res) => {
    const id = req.params.id ;
    console.log("/layouts/:id", id) ;
    Client.find({}).deleteOne({ "_id" : mongoose.Types.ObjectId(id) },function (err, data) {
        if (err) {
            res.status(500).json({ message: err.message })
        }
        console.log("removed:",data)
       //     // get the random function to get display based on client type.
        res.json({removed: data});
    });    
});

module.exports = router;