requires nodejs v10, mongodb 3.6 or higher, angular cli v 6.4 or higher  
run in client:  
npm i && npm run start  

run in server:  
npm i && npm run start  

open browser:  
http://localhost:4200  